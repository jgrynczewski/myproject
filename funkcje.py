fruit = ['apple', 'banana', 'orange']
price = [1.5, 3.0, 0.75]
#
# print(zip(fruit, price))
# print(list(zip(fruit, price)))
# print(dict(zip(fruit, price)))
#
# for item in zip(fruit, price):
#     print(item)
#
# LIST COMPREHENSION

word = "Napis"
a_list = []
for ch in word:
    a_list.append(ch)

print(a_list)
#
# a_list = [ ch+'a' for ch in word ]
# print(a_list)

a_list = [1, 2, 3, 4, 5]
# print([2*item for item in a_list])

n = 50
# print([ item*2 for item in range(n//2)])
#
# print( [item for item in range(n) if item%2 == 0] )

# DICT COMPREHENSION

user = ["Stefan", "Artur", "Zenobiusz", "Antonina", "Antek"]
phones  = ["111-222-333", "324-344-453", "123-342-432", "121-123-431", "123-454-123"]
#
# print({ key:value for key, value in zip(user, phones) if key.startswith("A")})

import math
import abc

class Shape(abc.ABC):
    @abc.abstractmethod
    def calculate_area(self):
        pass


    class Rectangle:
        def __init__(self, side_a, side_b):
            self.side_a = side_a
            self.side_b = side_b
        def calculate_area(self):
            return self.side_a * self.side_b

class Circle(Shape, Rectangle):
    def __init__(self, radius):
        self.radius = radius
    def calc_area(self):
        return self.radius * math.pi

c = Circle(5)