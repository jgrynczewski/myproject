import pytest

import math
import unittest

from func import circle_area

def test_area():
    assert circle_area(1) == math.pi
    assert circle_area(0) == 0
    assert circle_area(2.1) == math.pi * (2.1**2)

def test_values():
    with pytest.raises(ValueError):
        circle_area(-2)

def test_types():
    with pytest.raises(TypeError):
        circle_area("radius")

